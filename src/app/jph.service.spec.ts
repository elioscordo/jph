import { TestBed, inject } from '@angular/core/testing';

import { JphService } from './jph.service';

describe('JphService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [JphService]
    });
  });

  it('should be created', inject([JphService], (service: JphService) => {
    expect(service).toBeTruthy();
  }));
});
