export class Post {
    userId: number;
    id: number;
    title:String;
    body:String;
}

export class Id {
    id: number;
}