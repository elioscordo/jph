import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LandingComponent }  from './landing/landing.component';
import { PostsComponent }  from './posts/posts.component';


const routes: Routes =  [
  { path: '', component: LandingComponent  },
  { path: 'getposts', component: PostsComponent  },

];

@NgModule({
  imports: [ 
    RouterModule.forRoot(routes) 
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
