import { Component, OnInit } from '@angular/core';
import {JphService} from '../jph.service'

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  
  // Types of results 
  posts = []
  id  = null
  message = null
  noresultMessage = "Success. No results for this verb."

  constructor(private jphService: JphService) { }
  ngOnInit() {}
  
  resetResults(){
    this.posts = []
    this.id  = null
    this.message = null
  }

  get(){
    this.jphService.get().subscribe(posts => {
      this.resetResults()
      this.posts = posts
    })
  }
  
  post(){
    this.jphService.post().subscribe(id => {
      this.resetResults()
      this.id = id
    });
  }
  
  put(){
    this.jphService.put().subscribe(() => {
      this.resetResults()
      this.message = "Put: " +  this.noresultMessage
    })
  }
  
  delete(){
    this.jphService.delete().subscribe(() =>{
      this.resetResults()
      this.message = "Delete: " + this.noresultMessage
    })
  }

}
