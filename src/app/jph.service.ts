import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {Post , Id} from './post';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};


@Injectable({
  providedIn: 'root'
})
 export class JphService {
  private url1 = 'https://jsonplaceholder.typicode.com/posts';
  private url2 = 'https://jsonplaceholder.typicode.com/posts/1';

  constructor(private http: HttpClient ) { }
  
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  get(): Observable<Post[]> {
    return this.http.get<Post[]>(this.url1).pipe(
      catchError(this.handleError('get posts', []))
    );
  }
  
  post(): Observable<Id> {
    return this.http.post<Post>(this.url1, null, httpOptions)
  }

  put(): Observable<any> {
    return this.http.put(this.url2, null, httpOptions)
  }

  delete(): Observable<any>{
    return this.http.delete(this.url2, httpOptions)
  }


}
