# Jph

This is a client for the Json Placeholder Website. For more information visit https://jsonplaceholder.typicode.com/

## How to build and run the docker image.

Please find in the dist folder the optimized built of the project.

Within the project directory build the docker image:
`docker image build -t jph .`  

Check if the image is available (Optional):
`docker image ls`  

Run the  image:
`docker run -p 3000:80 --rm jph`

Navigate to  `http://localhost:3000`


A bit of documention:

## Routing 

The `app-routing` has been implemented with 2 pages.
`Landing` and `Posts`

## Networking 
Please find the networking in `jph.service.ts` in the src folder.

## Landing Component
Just a very simple page component to introduce the project scope

## Posts Component
A very basic component page that calls the fake endpoints

